
kepmodel documentation
======================

The kepmodel package allows to model radial velocity and/or astrometric time series,
including Keplerian orbits using `kepderiv <https://gitlab.unige.ch/Jean-Baptiste.Delisle/kepderiv>`_,
correlated noise / GP using `S+LEAF <https://gitlab.unige.ch/Jean-Baptiste.Delisle/spleaf>`_ (see [3]_),
and linear predictors (offets, drifts, activity indicators, etc.).
Planets can be easily searched for by computing a periodogram and its corresponding FAP (false alarm probability) analytically (see [2]_, [4]_).
The orbital elements can then be initialized from the periodogram (see [1]_, [4]_) to improve the fit convergence efficiency.

Installation
------------

Using conda
~~~~~~~~~~~

The kepmodel package can be installed using conda with the following command:

``conda install -c conda-forge kepmodel``

Using pip
~~~~~~~~~

It can also be installed using pip with:

``pip install kepmodel``

Usage
-----

The :doc:`_autosummary/kepmodel.rv.RvModel` class defines a radial velocity model,
the :doc:`_autosummary/kepmodel.astro.AstroModel` class defines an astrometric model,
and the :doc:`_autosummary/kepmodel.astrorv.AstroRvModel` allows to model both time series
at the same time.

A detailed example of the use of kepmodel (in the case of radial velocities)
can be found as a `DACE tutorial jupyter notebook <https://dace.unige.ch/pythonAPI/index.html?tutorialId=21>`_.

API Reference
-------------

.. autosummary::
   :toctree: _autosummary
   :template: autosummary/custom_module.rst
   :recursive:

   kepmodel.rv
   kepmodel.astro
   kepmodel.astrorv
   kepmodel.timeseries
   kepmodel.tools

References
----------

   .. [1] `Delisle et al., "Analytical determination of orbital elements using Fourier analysis. I. The radial velocity case", 2016 <http://adsabs.harvard.edu/abs/2016A\%26A...590A.134D>`_.
   .. [2] `Delisle et al., "Efficient modeling of correlated noise. I. Statistical significance of periodogram peaks", 2020 <https://ui.adsabs.harvard.edu/abs/2020A&A...635A..83D>`_.
   .. [3] `Delisle et al., "Efficient modeling of correlated noise. II. A flexible noise model with fast and scalable methods", 2020 <https://ui.adsabs.harvard.edu/abs/2020A\&A...638A..95D>`_.
   .. [4] Delisle et al., "Analytical determination of orbital elements using Fourier analysis. II. The astrometry case and combining with radial velocities", in prep.
